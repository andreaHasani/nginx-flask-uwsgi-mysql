## Setup for nginx-uwsgi+flask+mysql

There's two folders here which use different setup.
The single-container folder uses a setup which keeps app in a single container and uses nginx for reverse proxy
The multi-container folder keeps every app on different container and uses the nginx proxy to connect to them
